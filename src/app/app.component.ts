import { Component, OnInit} from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  users: any = [];

  constructor(protected userService: ApiService) {}

  ngOnInit() {
    this.fetchProducts();
  }

  fetchProducts() {
    this.userService.getUsers().subscribe(data => {
      this.users = data.results;
      console.log('DATOS PERSONA', this.users);
    });
  }

}
